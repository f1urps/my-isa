
#ifndef ltable_h
#define ltable_h

#include <stdbool.h>  // bool
#include <stdint.h>   // uint32_t

#include "uthash.h"   // UT_hash_handle

/**
 *  An address within the program's code, relative to the
 *  first byte of the program. For example, the address of an instruction
 *  or some static data.
 */
typedef uint32_t addr_t;

/**
 *  The maximum number of characters that can be involved in an identifier
 *  for a label
 */
#define LAB_LEN 32

/**
 *  A structure containing information about a label.
 *  This struct is used to store a mapping between label names
 *  and the addresses they refer to. This mapping is stored in a hash table,
 *  so this struct is hashable.
 */
typedef struct _label {
  char            name [LAB_LEN];  // Label name (used as key in hash table)
  addr_t          address;         // Program address referred to by label
  UT_hash_handle  hh;              // Makes the structure hashable
} _label;

/**
 *  Used to declare a hashtable of _labels.
 */
typedef _label* _ltable;

/**
 *  Add a new label to an ltable. There must not exist any label already in
 *  the table with the same name.
 */
bool ltable_add_label (_ltable* lt, char* label_name, addr_t label_addr);

/**
 *  Lookup an address in an ltable based on its name. If a label with the
 *  specified name is successfully found, true is returned and *lookup_result
 *  is assigned the value of its address. If no label is found, false is
 *  returned and *lookup_result is unmodified.
 */
bool ltable_lookup (_ltable* lt, char* lookup_name, addr_t* lookup_result);

/**
 *  Destroy the ltable and deallocate all label structs that were added to it.
 */
void ltable_free (_ltable* lt);


#endif  // ltable_h
