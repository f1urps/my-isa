
#include <stdbool.h>  // bool, true, false
#include <stdlib.h>   // malloc, free
#include <string.h>   // strncpy, strcmp

#include "ltable.h"   // _label, _ltable, addr_t
#include "uthash.h"   // HASH_FIND_STR, HASH_ADD_STR, HASH_ITER, HASH_DEL


/** ------------------
 *  ltable_add_label
 * -------------------
 *  see ltable.h for documentation
 */
bool ltable_add_label (_ltable* lt, char* label_name, addr_t label_addr) {
  _label* l = NULL;

  if (lt == NULL || label_name == NULL)
    return false;

  // Look for the label in the ltable
  HASH_FIND_STR (*lt, label_name, l);
  if (l != NULL)
    return false;
  // Allocate new struct
  l = (_label*) malloc (sizeof *l);              /**** malloc ****/
  l->address = label_addr;
  // Safely copy string into the new label struct
  strncpy (l->name, label_name, LAB_LEN);
  l->name[LAB_LEN-1] = 0;
  if (strcmp (l->name, label_name)) {
    free (l);
    return false;
  }
  HASH_ADD_STR (*lt, name, l);
}

/** ---------------
 *  ltable_lookup
 * ----------------
 *  see ltable.h for documentation
 */
bool ltable_lookup (_ltable* lt, char* lookup_name, addr_t* lookup_result) {
  _label* l = NULL;

  if (lt == NULL || lookup_name == NULL || lookup_result == NULL)
    return false;

  HASH_FIND_STR (*lt, lookup_name, l);
  if (l == NULL) {
    return false;
  } else {
    *lookup_result = l->address;
    return true;
  }
}

/** -------------
 *  ltable_free
 * --------------
 *  see ltable.h for documentation
 */
void ltable_free (_ltable* lt) {
  _label* curr = NULL;
  _label* tmp = NULL;
  if (lt != NULL) {
    HASH_ITER (hh, *lt, curr, tmp) {
      HASH_DEL (*lt, curr);
      free (curr);                                 /****  free  ****/
    }
  }
}
