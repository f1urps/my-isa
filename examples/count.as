
# Count to 10

# output:
# 1 2 3 4 5 6 7 8 9 10

.start
LDV  RA 10
LDV  RB 0

.lab loop
ADDV RB 1
PR   RB
PRCV ' '
CMP  RB RA
JNE  loop
HLT
