
# print hello world

# output:
# Hello World!

.lab str
.byte 'H'
.byte 'e'
.byte 'l'
.byte 'l'
.byte 'o'
.byte ' '
.byte 'W'
.byte 'o'
.byte 'r'
.byte 'l'
.byte 'd'
.byte '!'
.byte 0x00

.align 4
.start
LDV  RA str
LDV  RB 0

.lab loop
LDPB RB RA
CMPV RB 0
JE   done
PRC  RB
ADDV RA 1
JMP  loop

.lab done
HLT
