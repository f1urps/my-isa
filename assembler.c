
#include <stdbool.h>         // bool, true, false
#include <stdio.h>           // FILE, ...

#include "assembler.h"       // _header
#include "ltable.h"          // _ltable
#include "run-assembler.h"   // vout, err
#include "opcode.h"          // ...
#include "uthash.h"          // ...


// TODO: specific error messages for each function

/** --------------------
 *  assembler_assemble
 * ---------------------
 *  see assembler.h for documentation
 */
bool assembler_assemble (FILE* in, FILE* out, const bool v) {

  _ltable ltable = NULL;
  _header hdr;

  // Initialize opcode lookup system
  vout (v, "Initializing opcode and register lookup system.");
  opcode_init ();

  // Tabulate labels
  vout (v, "Tabulating labels...");
  if (! assembler_tabulate_labels (in, &ltable, &hdr, v)) {
    vout (v, "ERROR\n");
    err ("Label tabulation failed.");
    return false;
  }
  vout (v, "done.\n");

  // Output header
  vout (v, "Outputting header to output file...");
  if (! assembler_output_header (out, &hdr, v)) {
    vout (v, "ERROR\n");
    err ("Failed to output header. Output file is now possibly corrupted.");
    return false;
  }
  vout (v, "done.\n");

  // Output executable segment
  vout (v, "Outputting executable segment to output file...");
  if (! assembler_output_executable (in, out, &ltable, v)) {
    vout (v, "ERROR\n");
    err ("Failed to output executable segment. Output file is now possibly corrupted.");
    return false;
  }
  vout (v, "done.\n");

  // Free ltable
  vout (v, "Freeing label table.\n");
  ltable_free (&ltable);

  return true;
}

/** ---------------------------
 *  assembler_tabulate_labels
 * ----------------------------
 *  see assembler.h for documentation
 */
bool assembler_tabulate_labels (FILE* in, _ltable* ltable, _header* hdr, const bool v) {
  // TODO
  return true;
}

/** -------------------------
 *  assembler_output_header
 * --------------------------
 *  see assembler.h for documentation
 */
bool assembler_output_header (FILE* out, _header* hdr, const bool v) {
  // TODO
  return true;
}

/** -----------------------------
 *  assembler_output_executable
 * ------------------------------
 *  see assembler.h for documentation
 */
bool assembler_output_executable (FILE* in, FILE* out, _ltable* ltable, const bool v) {
  // TODO
  return true;
}
