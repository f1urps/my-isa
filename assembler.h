
#ifndef assembler_h
#define assembler_h

#include <stdio.h>    // FILE
#include <stdbool.h>  // bool

#include "ltable.h"   // _ltable


/**
 *  A header of an executable file.
 *  Contains metadata about a program that is necessary for execution, such
 *  as the start point of the program.
 *  Required by specification to be 32 bytes.
 */
typedef struct _header {
  addr_t          start_addr;   // Start address of the program
  char            UNUSED [28];  // Pad to 32 bytes
} _header;

/**
 *  Read assembly code from file in and assemble it into machine code
 *  in file out.
 *  FILE* in must be a file opened in character read mode.
 *  FILE* out must be a file opened in binary write or append mode.
 *  These files will NOT be closed by this function.
 *  Returns true on success, false on failure.
 */
bool assembler_assemble (FILE* in, FILE* out, const bool v);

/**
 *  Do a first pass over the assembly code, counting bytes to determine
 *  precise locations of all labels, as well as the .start directive.
 *  These locations are addresses relative to the start of the program
 *  and are stored in a global hash table for later use by output_executable().
 *  FILE* in must be a file opened in character read mode. Should already be
 *  seeked to the location to start reading.
 *  This file is NOT closed by this function.
 *  Returns true on success, false on failure.
 */
bool assembler_tabulate_labels (FILE* in, _ltable* ltable, _header* hdr, const bool v);

/**
 *  Output the file header of the executable.
 *  The header is 32 bytes. The first 4 bytes are the start address of
 *  the program.
 *  FILE* out should be a file opened in binary write or append mode.
 *  This file is NOT closed by this function.
 *  Returns true on success, false on failure.
 */
bool assembler_output_header (FILE* out, _header* hdr, const bool v);

/**
 *  Do a second pass over the assembly code, replacing labels with addresses
 *  as determined by the first pass (tabulate_labels()), and translate the
 *  program line-by-line into instructions and data, outputting to out as we go.
 *  FILE* in should be a file opened in character read mode. Should already be
 *  seeked to the location to start reading.
 *  FILE* out should be a file opened in binary write or append mode.
 *  Returns true on success, false on failure.
 */
bool assembler_output_executable (FILE* in, FILE* out, _ltable* ltable, const bool v);

#endif  // assembler_h
