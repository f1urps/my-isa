.DEFAULT_GOAL := all

assembler: opcode.h run-assembler.h run-assembler.c assembler.h assembler.c ltable.h ltable.c
	gcc assembler.c run-assembler.c ltable.c -o assembler

all: assembler

clean:
	rm -rf assembler
	rm -rf a.out
