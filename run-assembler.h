
#ifndef run_assembler_h
#define run_assembler_h

#include <stdbool.h>   // bool

/**
 *  Holds the values of various configuration options that can be set by
 *  command-line arguments.
 */
typedef struct _config_opts {
  char*      input_file;    // name of input file
  char*      output_file;   // name of output file
  bool       verbose;       // enable verbose output?
} _config_opts;

/**
 *  Parse arguments from command line and put them in the _config_opts struct.
 *  Takes argc and argv exactly as they are passed in to main, and fills config
 *  with values based on the command line arguments.
 */
void process_arguments (int argc, char** argv, _config_opts* config);

/**
 *  Prints a help message to standard output and exits with some exit code.
 */
void show_help (const int exit_code);

/**
 *  Prints a message to standard output exactly like cstdlib's printf, only
 *  if the parameter verbose is true. If verbose is false, this function does
 *  nothing.
 */
void vout (const bool verbose, const char* fmt, ...);

/**
 *  Prints an error message to standard error. The arguments should be the same
 *  as if one were calling cstdlib's printf. A newline is inserted after the
 *  formatted string, so none is necessary in fmt.
 */
void err (const char* fmt, ...);


#endif  // run_assembler_h
