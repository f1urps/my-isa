
#include <stdio.h>          // *printf, FILE, fopen, errno
#include <stdbool.h>        // bool
#include <unistd.h>         // getopt
#include <ctype.h>          // isprint
#include <stdlib.h>         // abort, exit
#include <assert.h>         // assert
#include <stdarg.h>         // va_list
#include <errno.h>          // errno
#include <string.h>         // strerror

#include "run-assembler.h"  // _config_opts
#include "assembler.h"      // assemble

// set by fopen if there is an error opening files
extern int errno;

/** ======
 *  main
 * =======
 */
int main (int argc, char** argv) {
  FILE* input_file = NULL;
  FILE* output_file = NULL;
  const char* const infile_access = "r";
  const char* const outfile_access = "w";
  bool assembly_success;

  _config_opts config = { NULL,     // input_file
                          NULL,     // output_file
                          false };  // verbose

  // parse options and arguments from command line
  process_arguments (argc, argv, &config);
  assert (config.input_file  != NULL);
  assert (config.output_file != NULL);
  vout (config.verbose, "Input file: %s\n", config.input_file);
  vout (config.verbose, "Output file: %s\n", config.output_file);

  // open input and output files
  vout (config.verbose, "Opening file %s with %s access, opening file %s with %s access...\n",
                  config.input_file, infile_access, config.output_file, outfile_access);
  input_file  = fopen (config.input_file, infile_access);
  output_file = fopen (config.output_file, outfile_access);

  // verify files were opened successfully
  if (!(input_file && output_file)) {
    if (!input_file)
      err ("Couldn't open input file %s: %s", config.input_file, strerror(errno));
    if (!output_file)
      err ("Couldn't open output file %s: %s", config.output_file, strerror(errno));
    exit(1);
  } else {
    vout (config.verbose, "Successfully opened files.\n");
  }

  // assemble from input_file to output_file
  vout (config.verbose, "Assembling.\n");
  assembly_success = assembler_assemble (input_file, output_file, config.verbose);

  // close files
  vout (config.verbose, "Closing files %s and %s.\n", config.input_file, config.output_file);
  fclose (input_file);
  fclose (output_file);

  // verify assembly succeeded
  if (!assembly_success) {
    err ("Assembly failed.");
    exit (1);
  }
  vout (config.verbose, "Assembly completed successfully.\n");
  exit (0);
}

/** -----------
 *  show_help
 * ------------
 *  see run-assembler.h for documentation.
 */
void show_help (const int exit_code) {
  printf ("Usage: assembler [-o outfile] [-hv] infile\n");
  exit (exit_code);
}

/** -------------------
 *  process_arguments
 * --------------------
 *  see run-assembler.h for documentation.
 */
void process_arguments (int argc, char** argv, _config_opts* config) {

   int index;
   int c;

   // Currently supported arguments:
   //   -h     shows help info and exits
   //   -v     enable verbose output
   //   -o <f> give name to output file
   const char* const available_opts = "hvo:";

   assert (argv    != NULL);
   assert (config  != NULL);

   // Parse arguments with getopt
   opterr = 0;
   while ((c = getopt (argc, argv, available_opts)) != -1)
     switch (c)
       {
       case 'h':
         show_help(0);
         break;  // unreached

       case 'o':
         config->output_file = optarg;
         break;

       case 'v':
         config->verbose = true;
         break;

       case '?':
         if (optopt == 'o')
           err ("Option -%c requires an argument.", optopt);
         else if (isprint (optopt))
           err ("Unknown option `-%c'.", optopt);
         else
           err ("Unknown option character `\\x%x'.", optopt);
         show_help(1);
         break;  // unreached

       default:
         abort ();
         break;  // unreached

       }

   // Verify the correct number of remaining arguments,
   // Set input file name as first non-option argument
   if (optind >= argc) {
     err ("Missing input file argument.");
     show_help (1);
   } else if (argc > optind + 1) {
     err ("Too many arguments.");
     show_help (1);
   } else {
     config->input_file = argv[optind];
   }

   // If -o option was not provided, use default filename
   if (config->output_file == NULL) {
     config->output_file = "a.out";
   }

   assert (config->input_file  != NULL);
   assert (config->output_file != NULL);
}

/** ----------------
 *  vout
 * -----------------
 *  see run-assembler.h for documentation.
 */
void vout (const bool verbose, const char* fmt, ...) {
  if (verbose) {
    va_list args;
    va_start (args, fmt);
    vprintf (fmt, args);
    va_end (args);
  }
}

/** ------------
 *  err
 * -------------
 *  see run-assembler.h for documentation.
 */
void err (const char* fmt, ...) {
  va_list args;
  va_start (args, fmt);
  fprintf (stderr, "*** ERROR *** ");
  vfprintf (stderr, fmt, args);
  fprintf (stderr, "\n");
  va_end (args);
}
