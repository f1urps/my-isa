
#ifndef opcode_h
#define opcode_h

#include <stdbool.h>    // bool
#include <stdint.h>     // uint8_t
#include <string.h>     // strcmp

#include "uthash.h"     // UT_hash_handle, ...

// TODO: more detailed comments

// 8 bits represent an opcode
typedef uint8_t opcode_t;

// 8 bits to represent a register
typedef uint8_t reg_t;

// Metadata about an instruction
typedef struct _instr_info {
  char name[5];                 // pneumonic
  opcode_t opcode;              // opcode
  bool takes_register_main;     // takes main register arg?
  bool takes_register_data;     // takes second register arg?
  bool takes_value;             // takes value arg?
  UT_hash_handle hh;            // makes the struct hashable
} _instr_info;

// Metadata about a register
typedef struct _reg_info {
  char name[3];                 // pneumonic
  reg_t code;                   // register code
  bool read_only;               // is read-only?
  UT_hash_handle hh;            // makes the struct hashable
} _reg_info;

// Error codes (used internally)
static opcode_t OPCODE_ERROR = 0xff;
static reg_t REG_ERROR = 0xff;

// Hash tables for quickly looking up opcodes and registers
static _instr_info* opcode_table = NULL;
static _reg_info* reg_table = NULL;

//*********//
/* OPCODES */
//*********//

// TODO: make this more efficient with a hash table instead of an array

// name, opcode, has first register arg, has second register arg, has data arg
static _instr_info ops[] = {
// General
{"NOP",    0x00, 0,0,0},  // no-op
{"HLT",    0x01, 0,0,0},  // halt
// Load
{"LD",     0x10, 1,0,1},  // load from address
{"LDB",    0x11, 1,0,1},  // load from address -- byte
{"LDR",    0x12, 1,1,0},  // load from register
{"LDV",    0x13, 1,0,1},  // load from value
{"LDVB",   0x14, 1,0,1},  // load from value -- byte
{"LDP",    0x15, 1,1,0},  // load from address in register
{"LDPB",   0x16, 1,1,0},  // load from address in register -- byte
// Store
{"SR",     0x20, 1,0,1},  // store to address
{"SRB",    0x21, 1,0,1},  // store to address -- byte
{"SRP",    0x22, 1,1,0},  // store to address in register
{"SRPB",   0x23, 1,1,0},  // store to address in register -- byte
// Compare
{"CMP",    0x30, 1,1,0},  // compare reg/reg
{"CMPV",   0x31, 1,0,1},  // compare reg/value
// Jump
{"JMP",    0x40, 0,0,1},  // jump
{"JE",     0x41, 0,0,1},  // jump if equal
{"JNE",    0x42, 0,0,1},  // jump if not equal
{"JG",     0x43, 0,0,1},  // jump if greater
{"JL",     0x44, 0,0,1},  // jump if lesser
// Arithmetic
{"ADD",    0x50, 1,1,0},  // add
{"ADDV",   0x51, 1,0,1},  // add value
{"SUB",    0x52, 1,1,0},  // subtract
{"SUBV",   0x53, 1,0,1},  // subtract value
{"MUL",    0x54, 1,1,0},  // multiply
{"MULV",   0x55, 1,0,1},  // multiply value
{"DIV",    0x56, 1,1,0},  // divide
{"DIVV",   0x57, 1,0,1},  // divide value
// Bitwise
{"NOT",    0x60, 1,0,0},  // negate
{"AND",    0x61, 1,1,0},  // and
{"ANDV",   0x62, 1,0,1},  // and value
{"OR",     0x63, 1,1,0},  // or
{"ORV",    0x64, 1,0,1},  // or value
{"XOR",    0x65, 1,1,0},  // xor
{"XORV",   0x66, 1,0,1},  // xor value
{"SHR",    0x67, 1,1,0},  // shift right
{"SHRV",   0x68, 1,0,1},  // shift right value
{"SHL",    0x69, 1,1,0},  // shift left
{"SHLV",   0x6A, 1,0,1},  // shift left value
// Subroutines
{"CALL",   0x70, 0,0,1},  // call
{"RET",    0x71, 0,0,0},  // return
// Stack
{"PSH",    0x80, 1,0,0},  // push
{"PSHB",   0x81, 1,0,0},  // push -- byte
{"PSHV",   0x82, 0,0,1},  // push value
{"PSVB",   0x83, 0,0,1},  // push value -- byte
{"POP",    0x84, 1,0,0},  // pop
{"POPB",   0x85, 1,0,0},  // pop -- byte
// I/O
{"PR",     0x90, 1,0,0},  // print
{"PRX",    0x91, 1,0,0},  // print hex
{"PRC",    0x92, 1,0,0},  // print character -- byte
{"PRV",    0x93, 0,0,1},  // print value
{"PRXV",   0x94, 0,0,1},  // print hex value
{"PRCV",   0x95, 0,0,1},  // print character value -- byte
{"RD",     0x96, 1,0,0},  // read
{"RDB",    0x97, 1,0,0}}; // read -- byte

//***********//
/* REGISTERS */
//***********//

// name, code, is read-only
static _reg_info regs[] = {
// Specialty
{"IP",     0x00, 1},
{"SP",     0x01, 0},
{"BP",     0x02, 0},
// General Purpose
{"RA",     0x10, 0},
{"RB",     0x11, 0},
{"RC",     0x12, 0},
{"RD",     0x13, 0},
{"RE",     0x14, 0},
{"RF",     0x15, 0},
// Flags
{"ZF",     0x20, 1},
{"NF",     0x21, 1},
{"OF",     0x22, 1},
{"IF",     0x23, 1}};

/**
 *  Initialize opcode and register hash tables. Must be called once before
 *  any other opcode function.
 */
void opcode_init () {
  int num_instr = sizeof (ops) / sizeof (ops[0]);
  int num_reg = sizeof (regs) / sizeof (regs[0]);
  int i = 0;

  for (i = 0; i < num_instr; ++i) {
    HASH_ADD_STR (opcode_table, name, &(ops[i]));
  }
  for (i = 0; i < num_reg; ++i) {
    HASH_ADD_STR (reg_table, name, &(regs[i]));
  }
}

/**
 *  Given a pneumonic for an operation (e.g. NOP, LD, MUL, etc.)
 *  find and return the corresponding opcode. If flags is non-null,
 *  useful information is stored at that address, which
 *  can be extracted with the functions: op_takes_reg_main(),
 *  op_takes_reg_data(), and op_takes_value(), explained below.
 */
opcode_t opcode_ptoop (char* pneumonic, int* flags) {
  _instr_info* instr = NULL;
  if (pneumonic == NULL)
    return OPCODE_ERROR;
  HASH_FIND_STR (opcode_table, pneumonic, instr);
  if (instr == NULL)
    return OPCODE_ERROR;

  if (flags != NULL) {
    *flags = 0;
    *flags |= instr->takes_value;
    *flags |= instr->takes_register_data << 1;
    *flags |= instr->takes_register_main << 2;
  }
  return instr->opcode;
}

/**
 *  Given a pneumonic for a register (e.g. IP, RA, ZF, etc.)
 *  find and return the corresponding register code. If flags is non-null,
 *  useful information about the register is stored at that address,
 *  which can be extracted via the function: reg_read_only(), explained below.
 */
reg_t opcode_ptoreg (char* pneumonic, int* flags) {
  _reg_info* reg = NULL;
  if (pneumonic == NULL)
    return REG_ERROR;
  HASH_FIND_STR (reg_table, pneumonic, reg);
  if (reg == NULL)
    return REG_ERROR;

  if (flags != NULL) {
    *flags = 0;
    *flags |= reg->read_only;
  }
  return reg->code;
}

/**
 *  Decodes flag data obtained by opcode_from_pneumonic().
 *  Returns true iff the operation takes a main register argument.
 */
bool opcode_takes_regmain (int op_flags) {
  return op_flags & 0x4;
}

/**
 *  Decodes flag data obtained by opcode_from_pneumonic().
 *  Returns true iff the operation takes a secondary register argument
 *  in the data segment.
 */
bool opcode_takes_regdata (int op_flags) {
  return op_flags & 0x2;
}

/**
 *  Decodes flag data obtained by opcode_from_pneumonic().
 *  Returns true iff the operation takes a value argument.
 */
bool opcode_takes_value (int op_flags) {
  return op_flags & 0x1;
}

/**
 *  Decodes flag data obtained by register_from_pneumonic().
 *  Returns true iff the register is read-only.
 */
bool opcode_read_only (int reg_flags) {
  return reg_flags & 0x1;
}


#endif   // opcode_h
